/**
 * File: src/SEA/SeaOptions.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/17/14		hcai		created; for command-line argument processing for SeaAnalysis 
 *
*/
package SEA;

import java.util.ArrayList;
import java.util.List;

public class SeaOptions {
	protected boolean debugOut = false;
	protected boolean dumpJimple = false;
	protected boolean dumpFunctionList = false;
	protected boolean visualize = false;
	protected boolean queryAll = false;
	protected String query = "";
	
	/* if constructing ICFG */
	protected boolean queryOnICFG = false;
	/* if visualizing ICFG */
	protected boolean visualizeICFG = false;
	
	public boolean queryOnICFG() { return queryOnICFG; }
	public boolean visualizeICFG() { return visualizeICFG; }

	public boolean debugOut() { return debugOut; }
	public boolean dumpJimple() { return dumpJimple; }
	public boolean dumpFunctionList() { return dumpFunctionList; }
	public boolean visualize() { return visualize; }
	public boolean queryAll() { return queryAll; }
	public String query() { return query; }
		
	public String[] process(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		boolean allowPhantom = true;
		
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];

			if (arg.equals("-debug")) {
					debugOut = true;
			}
			else if (arg.equals("-dumpJimple")) {
				dumpJimple = true;
			}
			else if (arg.equals("-visualize")) {
				visualize = true;
			}
			else if (arg.equals("-dumpFunctionList")) {
				dumpFunctionList = true;
			}
			else if (arg.equals("-nophantom")) {
				allowPhantom = false;
			}
			else if (arg.equals("-query")) {
				// give the changed methods (to be queried), separated by comma if there are more than one
				query = arg;
			}
			else if (arg.equals("-queryAll")) {
				// query the entire program (all methods)
				queryAll = true;
			}
			else if (arg.equals("-serializeICFG")) {
				visualizeICFG = true;
			}
			else if (arg.equalsIgnoreCase("-queryOnICFG")) {
				queryOnICFG = true;
			}
			else {
				argsFiltered.add(arg);
			}
		}
		
		if (allowPhantom) {
			argsFiltered.add("-allowphantom");
		}
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return (String[]) argsFiltered.toArray(arrArgsFilt);
	}
}

/* vim :set ts=4 tw=4 tws=4 */

