/**
 * File: src/SEA/MethodFwdSlice.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 01/12/15		hcai		created; for computing method-level static forward slice to serve as
 *							the ground truth of static IAs (SEA and MDG)
 * 01/13/15		hcai		done coding; to be used and tested
 * 01/14/15		hcai		fix bugs revealed when getting it used on NanoXML: skip special (ENTRY/EXIT) and exceptional units
 * 							as slice criterion when computing forwad static slices 
 * 01/21/15		hcai		added multithreaded and random-sampling options for speeding up the slicer
*/
package SEA;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import MciaUtil.utils;
import dua.Extension;
import dua.Forensics;
import dua.global.ProgramFlowGraph;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFG;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.Variable;
import fault.StmtMapper;

public class MethodFwdSlice implements Extension {
	
	protected static SeaOptions opts = new SeaOptions();
	
	// collect all methods in order to filter out library functions in the resulting impact sets
	protected final static Set<String> allAppFuncs = new HashSet<String>(); 
	
	static Set<String> changeSet = new LinkedHashSet<String>();
	static Map<String, Set<String> > impactSets = new LinkedHashMap<String, Set<String>>();
	
	static Map<String, CFG> mToCfgs = new LinkedHashMap<String, CFG>();
	
	static boolean multithread = false, randomselect=true;
	static Random r = new Random();
	
	public static void main(String args[]){
		args = preProcessArgs(opts, args);
		r.setSeed(System.currentTimeMillis());

		MethodFwdSlice sea = new MethodFwdSlice();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		
		Forensics.registerExtension(sea);
		Forensics.main(args);
	}
	
	protected static String[] preProcessArgs(SeaOptions _opts, String[] args) {
		opts = _opts;
		args = opts.process(args);
		
		String[] argsForDuaF;
		int offset = 0;

		argsForDuaF = new String[args.length + 2 - offset];
		System.arraycopy(args, offset, argsForDuaF, 0, args.length-offset);
		argsForDuaF[args.length+1 - offset] = "-paramdefuses";
		argsForDuaF[args.length+0 - offset] = "-keeprepbrs";
		
		return argsForDuaF;
	}
	
	@Override public void run() {
		System.out.println("Running MethodFwdSlice extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		utils.getFunctionList(allAppFuncs);
		
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			mToCfgs.put(cfg.getMethod().getSignature(), cfg);
		}
		
		// process specified queries
		if (!opts.query().isEmpty()) {
			// queryAll();
			startProcessQueries(opts.query());
			return;
		}
		
		if (!opts.queryAll()) {
			return;
		}
		
		if (multithread) {
			querySingle (allAppFuncs.iterator().next()); // warm up; done global control and data-flow analysis
			int N = ((allAppFuncs.size()<100) ? 1:100);
			List<slicer> ats = new ArrayList<slicer>();
			for (int i = 0; i < N; i++) {
				slicer sl = new slicer(N, i);
				ats.add(sl);
				sl.start();
			}
			for (int i = 0; i < N; i++) {
				try {
					ats.get(i).join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return;
		}
		
		Map<String, Set<String>> allResults = new HashMap<String, Set<String>>();
		Map<String, Long> allTimecosts = new HashMap<String, Long>();
		int icnt = 1;
		
		if (randomselect) {
			int N = Math.min(300, allAppFuncs.size());
			List<String> selection = new ArrayList<String>(allAppFuncs);
			int sz = selection.size();
			List<Integer> used = new ArrayList<Integer>();
			for (int i = 0; i < N; i++) {
				int k = Math.abs(r.nextInt() % sz);
				while (used.contains(k)) {
					k = Math.abs(r.nextInt() % sz);
				}
				used.add(k);
			}
			System.out.println("Done selecting " + N + " samples randomly.");
			for (int j : used) {
				String m = selection.get(j);
				System.out.println("sc: " + m + " [ " + (icnt++) + "/" + N + " ]");
				long st = System.currentTimeMillis();
				allResults.put(m, querySingle(m));
				allTimecosts.put(m, System.currentTimeMillis() - st);
			}
		}
		else { 
			// or process all possible queries
			for (String m : allAppFuncs) {
				// testing
				if (opts.debugOut()) {
					System.out.println("True impact-set of " + m + " is following:\n\t " + querySingle(m));
				}
				System.out.println("sc: " + m + " [ " + (icnt++) + "/" + allAppFuncs.size() + " ]");
				long st = System.currentTimeMillis();
				allResults.put(m, querySingle(m));
				allTimecosts.put(m, System.currentTimeMillis() - st);
			}
		}
		// serialize the results
		String sfn = dua.util.Util.getCreateBaseOutPath() + "sliceResults.dat";
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(sfn);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(allResults);
			oos.writeObject(allTimecosts);
			oos.flush();
			oos.close();
			System.out.println("All static-slice query results have been serialized to " + sfn);
		}
		catch (Exception e) {
			System.err.println("Error occurred during the serialization of all static-slice query results.");
			e.printStackTrace();
		}
	}
	
	/** find all methods having SEA relation with f as the impacted method of f */
	public static Set<String> querySingle(String f) {
		Set<String> S = new HashSet<String>();
		if (!mToCfgs.containsKey(f)) {
			System.err.println("Invalid query, nothing to do.");
			return S;
		}
		
		CFG cfg = mToCfgs.get(f);
		Set<NodePoint> scNodes = new HashSet<NodePoint>();
		for (CFGNode n : cfg.getNodes()) {
			if (n.isInCatchBlock() || n.isSpecial()) continue;
			boolean hasPre = false;
			List<Variable> defs = ((NodeDefUses)n).getDefinedVars();
			if (n.getSuccs().size()<=1 && !defs.isEmpty()) {
				for (Variable vdef : defs) {
					if (vdef.isStrConstObj()) { hasPre = true; break; }
					
					// this test is expensive - we assume there are always uses of the def
					//if (!DependenceFinder.getAllUsesForDef(vdef, cfgNode).isEmpty())
					{
						if	(n.getCallSite() != null && n.getCallSite().hasAppCallees()) {
							hasPre = true; break;
						}
					}
				}
			}
			if (hasPre) {
				scNodes.add(new NodePoint(n, NodePoint.PRE_RHS));
			}
			scNodes.add(new NodePoint(n, NodePoint.POST_RHS));
		}
		for (NodePoint np : scNodes) {
			DependenceGraph depGraph = new DependenceGraph(np, -1);
			for (NodePoint scn : depGraph.getPointsInSlice()) {
				S.add( ProgramFlowGraph.inst().getContainingMethod(scn.getN()).getSignature() );
			}
		}
		
		S.retainAll(allAppFuncs);
		return S;
	}
	
	public static int obtainValidChangeSet(String changedMethods) {
		changeSet.clear();  // in case this method gets multiple invocations from external callers 
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return -1;
		}
		// determine the valid change set
		Set<String> validChgSet = new LinkedHashSet<String>();
		for (String chg : Chglist) {
			validChgSet.add(chg);
		}
		if (validChgSet.isEmpty()) {
			// nothing to do
			// System.out.println("Invalid queries, nothing to do.");
			return 0;
		}
		changeSet.addAll(validChgSet);
		return changeSet.size();
	}
	public static Set<String> getChangeSet() {
		return changeSet;
	}
	
	public static Set<String> queryAll() {
		Set<String> S = new HashSet<String>();
		
		List<String> Chglist = dua.util.Util.parseStringList(opts.query(), ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return S;
		}
		
		for (String f : Chglist) {
			S.addAll(querySingle(f));
		}
		return S;
	}
	
	private static void printStatistics (Map<String, Set<String>> mis, boolean btitle) {
		if (btitle) {
			System.out.println("\n============ Static-slicing Result ================");
			System.out.println("[Valid Change Set]");
			for (String m:changeSet) {
				System.out.println(m);
			}
		}
		Set<String> aggregatedIS = new LinkedHashSet<String>();
		for (String m : mis.keySet()) {
			System.out.println("[Change Impact Set of " + m + "]: size= " + mis.get(m).size());
			for (String im : mis.get(m)) {
				System.out.println(im);
			}
			// merge impact sets of all change queries
			aggregatedIS.addAll(mis.get(m));
		}
		if (btitle) {
			System.out.println("\n[Change Impact Set of All Changes]: size= " + aggregatedIS.size());
			for (String im : aggregatedIS) {
				System.out.println(im);
			}
		}
	}
	
	public static void startProcessQueries(String changedMethods) {
		impactSets.clear();
		
		int nret = obtainValidChangeSet(changedMethods);
		if ( nret <= 0 ) {
			// nothing to do
			if (nret == 0) {
				// always output report so that post-processing script can work with the Diver result in a consistent way as if there were
				// some non-empty results
				printStatistics(impactSets, true);
			}
			return;
		}
		
		for (String chg : changeSet) {
			Set<String> singleImpactSet = querySingle(chg);
			
			if (impactSets.get(chg) == null) {
				impactSets.put(chg, new LinkedHashSet<String>());
			}
			impactSets.get(chg).addAll(singleImpactSet);
		}
		
		printStatistics(impactSets, true);
	}
	
	class slicer extends Thread
	{
	   int tid = 0;
	   int nparts = 1;
	   slicer(int _nparts, int partid)
	   {
	     super("slicer thread #" + partid);
	     tid = partid;
	     nparts = _nparts;
	   }
	   public void run()
	   {
		try {
			System.setOut(new PrintStream(new FileOutputStream(new File(dua.util.Util.getCreateBaseOutPath() + "sliceLog" + tid))));
			System.setErr(new PrintStream(new FileOutputStream(new File(dua.util.Util.getCreateBaseOutPath() + "sliceErr" + tid))));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
	    Map<String, Set<String>> allResults = new HashMap<String, Set<String>>();
		Map<String, Long> allTimecosts = new HashMap<String, Long>();
		int icnt = 0;
		int nstart = (allAppFuncs.size()/nparts)*tid, nend = nstart + allAppFuncs.size()/nparts; 
		if (allAppFuncs.size()%nparts!=0 && tid == nparts-1) {
			nend = allAppFuncs.size();
		}
		 for (String m : allAppFuncs) {
			if (!(icnt >= nstart && icnt < nend)) {
				icnt ++;
				continue; 
			}
			// testing
			if (opts.debugOut()) {
				System.out.println("True impact-set of " + m + " is following:\n\t " + querySingle(m));
			}
			System.out.println("sc: " + m + " [ " + (++icnt) + "/" + allAppFuncs.size() + " ]");
			long st = System.currentTimeMillis();
			allResults.put(m, querySingle(m));
			allTimecosts.put(m, System.currentTimeMillis() - st);
		}
		// serialize the results
		String sfn = dua.util.Util.getCreateBaseOutPath() + "sliceResults.dat" + tid;
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(sfn);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(allResults);
			oos.writeObject(allTimecosts);
			oos.flush();
			oos.close();
			System.out.println("All static-slice query results have been serialized to " + sfn);
		}
		catch (Exception e) {
			System.err.println("Error occurred during the serialization of all static-slice query results.");
			e.printStackTrace();
		}
	     System.out.println("thread " + tid + " run is over" );
	   }
	}
} // -- public class MethodFwdSlice  

/* vim :set ts=4 tw=4 tws=4 */

