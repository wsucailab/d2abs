/**
 * File: src/distEA/distdistSocketOutputStream.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      	Changes
 * -------------------------------------------------------------------------------------------
 * 02/11/15		hcai			Created; for intercepting socket outputs
*/
package distEA;

import java.io.*;
import java.lang.management.ManagementFactory;

import distEA.distMonitor.logicClock;
//import distEA.distThreadMonitor.logicClock;

/* customized Socket OutputStream where extraneous operations, for retrieving logic clocks, are added
 */
/** for replacing socket output stream objects */
public class distSocketOutputStream extends OutputStream {
	public static void __link() { }
	
	public static boolean intercept = true;
	
	private OutputStream out;
	private logicClock lgclock;

	public distSocketOutputStream(OutputStream out, logicClock clock) {
		this.out = out;
		this.lgclock = clock;
	}
	public distSocketOutputStream(OutputStream out) {
		this(out, distMonitor.g_lgclock);
		//this(out, distThreadMonitor.getCreateClock());
	}

	@Override
	public void close() throws IOException {
		out.close();
	}

	@Override
	public void flush() throws IOException {
		out.flush();
	}

	@Override
	public void write(byte[] b) throws IOException {
		this.write(b, 0, b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		System.out.println("******************** distSocketOutputStream write() byte b="+b+" getProcessID = " + getProcessID()+ " timestamp="+System.currentTimeMillis());		
		if (intercept) {
			lgclock.packClock(out, len);
		}
		out.write(b, off, len);
	}

	@Override
	public void write(int b) throws IOException {
		System.out.println("******************** distSocketOutputStream write() int b="+b+" getProcessID = " + getProcessID()+ " timestamp="+System.currentTimeMillis());		
		if (intercept) {
			lgclock.packClock(out, 4);
		}
		out.write(b);
	}
	public static String getProcessID() {
		return ManagementFactory.getRuntimeMXBean().getName()+'\0';
	}
}
	
/* vim :set ts=4 tw=4 tws=4 */

