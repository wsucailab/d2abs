package Diver;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ksu.cis.indus.common.datastructures.Triple;
import edu.ksu.cis.indus.common.datastructures.Pair.PairManager;
import edu.ksu.cis.indus.common.soot.SootBasedDriver;
import edu.ksu.cis.indus.interfaces.ICallGraphInfo;
import edu.ksu.cis.indus.interfaces.IEnvironment;
import edu.ksu.cis.indus.interfaces.IEscapeInfo;
import edu.ksu.cis.indus.interfaces.IMonitorInfo;
import edu.ksu.cis.indus.interfaces.IThreadGraphInfo;
import edu.ksu.cis.indus.processing.IProcessor;
import edu.ksu.cis.indus.processing.OneAllStmtSequenceRetriever;
import edu.ksu.cis.indus.processing.TagBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.callgraphs.CallGraphInfo;
import edu.ksu.cis.indus.staticanalyses.callgraphs.OFABasedCallInfoCollector;
import edu.ksu.cis.indus.staticanalyses.cfg.CFGAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.DeadlockAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.MonitorAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.escape.EquivalenceClassBasedEscapeAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.escape.LockAcquisitionBasedEquivalence;
import edu.ksu.cis.indus.staticanalyses.flow.instances.ofa.OFAnalyzer;
import edu.ksu.cis.indus.staticanalyses.flow.processors.ThreadGraph;
import edu.ksu.cis.indus.staticanalyses.interfaces.IValueAnalyzer;
import edu.ksu.cis.indus.staticanalyses.processing.AnalysesController;
import edu.ksu.cis.indus.staticanalyses.processing.CGBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.processing.ValueAnalyzerBasedProcessingController;
import edu.ksu.cis.indus.staticanalyses.tokens.ITokens;
import edu.ksu.cis.indus.staticanalyses.tokens.TokenUtil;
import edu.ksu.cis.indus.staticanalyses.tokens.soot.SootValueTypeManager;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Value;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.spark.SparkTransformer;
import soot.options.Options;

public class DiverTestDeadLock extends SootBasedDriver  {
	public static String path = "";
	static boolean debugOut = false;
	private static IValueAnalyzer<Value> analyzer;
	public static void main(String args[]){
		if (args.length < 1) {
			System.err.println("Too few arguments: \n\t ");
			return;
		}
		path = args[0]; // 
		if (args.length > 1) {
			debugOut = args[1].equalsIgnoreCase("-debug");
		}
		initial(path);
		enableSpark(path);
		Collection<String> classes= new ArrayList<String>();
		/* traverse all application classes */
		for (SootClass sClass:Scene.v().getApplicationClasses()) 
		{
			classes.add(sClass.toString());
		}	

		if (debugOut)
			System.out.println("classes="+classes);	
		DiverTestDeadLock _dtg = new DiverTestDeadLock();
		_dtg.setClassNames(classes);			
		_dtg.<ITokens> execute();
	}	
	/**
	 * This contains the driver logic.
	 * 
	 * @param <T> dummy type parameter.
	 */
	private <T extends ITokens<T, Value>> void execute() {
		//setInfoLogger(LOGGER);
		String _tagName = "SideEffect:FA";
		IValueAnalyzer<Value> _aa = OFAnalyzer.getFSOSAnalyzer(_tagName, TokenUtil
				.<T, Value, Type> getTokenManager(new SootValueTypeManager()), getStmtGraphFactory());
		ValueAnalyzerBasedProcessingController _pc = new ValueAnalyzerBasedProcessingController();
		Collection<IProcessor> _processors = new ArrayList<IProcessor>();
		PairManager _pairManager = new PairManager(false, true);
		CallGraphInfo _cgi = new CallGraphInfo(new PairManager(false, true));
		MonitorAnalysis _monitorInfo = new MonitorAnalysis();
		OFABasedCallInfoCollector _callGraphInfoCollector = new OFABasedCallInfoCollector();
		IThreadGraphInfo _tgi = new ThreadGraph(_cgi, new CFGAnalysis(_cgi, getBbm()), _pairManager);
		ValueAnalyzerBasedProcessingController _cgipc = new ValueAnalyzerBasedProcessingController();
		OneAllStmtSequenceRetriever _ssr = new OneAllStmtSequenceRetriever();

		_ssr.setStmtGraphFactory(getStmtGraphFactory());

		_pc.setStmtSequencesRetriever(_ssr);
		_pc.setAnalyzer(_aa);
		_pc.setProcessingFilter(new TagBasedProcessingFilter(_tagName));

		_cgipc.setAnalyzer(_aa);
		_cgipc.setProcessingFilter(new CGBasedProcessingFilter(_cgi));
		_cgipc.setStmtSequencesRetriever(_ssr);

		Map _info = new HashMap();
		_info.put(ICallGraphInfo.ID, _cgi);
		_info.put(IThreadGraphInfo.ID, _tgi);
		_info.put(PairManager.ID, _pairManager);
		_info.put(IEnvironment.ID, _aa.getEnvironment());
		_info.put(IValueAnalyzer.ID, _aa);

		initialize();
		_aa.analyze(getEnvironment(), getRootMethods());

		_processors.clear();
		_processors.add(_callGraphInfoCollector);
		_pc.reset();
		_pc.driveProcessors(_processors);
		_cgi.reset();
		_cgi.createCallGraphInfo(_callGraphInfoCollector.getCallInfo());
		writeInfo("CALL GRAPH:\n" + _cgi.toString());
		if (debugOut)
			System.out.println("CALL GRAPH:\n" + _cgi.toString());
		
		_processors.clear();
		((ThreadGraph) _tgi).reset();
		_processors.add((IProcessor) _tgi);
		_cgipc.reset();
		_cgipc.driveProcessors(_processors);
		writeInfo("THREAD GRAPH:\n" + ((ThreadGraph) _tgi).toString());
		if (debugOut)
			System.out.println("THREAD GRAPH:\n" + ((ThreadGraph) _tgi).toString());
		final EquivalenceClassBasedEscapeAnalysis _ecba = new EquivalenceClassBasedEscapeAnalysis(_cgi, _tgi, getBbm());
		final IEscapeInfo _escapeInfo = _ecba.getEscapeInfo();
		final AnalysesController _ac = new AnalysesController(_info, _cgipc, getBbm());
		_ac.addAnalyses(EquivalenceClassBasedEscapeAnalysis.ID, Collections.singleton(_ecba));
		_ac.addAnalyses(IMonitorInfo.ID, Collections.singleton(_monitorInfo));
		_ac.initialize();
		_ac.execute();
		writeInfo("END: Escape analysis");

		final LockAcquisitionBasedEquivalence _lbe = new LockAcquisitionBasedEquivalence(_escapeInfo, _cgi);
		_lbe.hookup(_cgipc);
		_cgipc.process();
		_lbe.unhook(_cgipc);

		System.out.println("Deadlock Analysis:");
		System.out.println("Total number of monitors: " + _monitorInfo.getMonitorTriples().size());
		calculateDeadlockInfo(_aa, _monitorInfo, null, null);
		calculateDeadlockInfo(_aa, _monitorInfo, _escapeInfo, null);
		calculateDeadlockInfo(_aa, _monitorInfo, null, _lbe);
		calculateDeadlockInfo(_aa, _monitorInfo, _escapeInfo, _lbe);
	}	
	// soot option 1
	private static void initial(String classPath) {
		soot.G.reset();
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_process_dir(Collections.singletonList(classPath));//
		Options.v().set_whole_program(true);
		Scene.v().loadNecessaryClasses();
		
	}
	
	// soot option 2
    private static void enableSpark(String path){
        HashMap opt = new HashMap();
        //opt.put("verbose","true");
        //opt.put("propagator","worklist");
        opt.put("simple-edges-bidirectional","false");
        //opt.put("on-fly-cg","true");
        opt.put("apponly", "true");
//        opt.put("set-impl","double");
//        opt.put("double-set-old","hybrid");
//        opt.put("double-set-new","hybrid");
//        opt.put("allow-phantom-refs", "true");
        opt.put("-process-dir",path);
        
        SparkTransformer.v().transform("",opt);
    }
	/**
	 * Calculates deadlock information based on the given inputs.
	 * 
	 * @param aa is the OFA to be used.
	 * @param monitorInfo is the monitor analysis to be used.
	 * @param escapeInfo is the escape analysis to be used.
	 * @param lbe is the lock based equivalence analysis to be used.
	 */
	private void calculateDeadlockInfo(final IValueAnalyzer<Value> aa, final MonitorAnalysis monitorInfo,
			final IEscapeInfo escapeInfo, final LockAcquisitionBasedEquivalence lbe) {
		final DeadlockAnalysis _dla = new DeadlockAnalysis(monitorInfo, aa, lbe, escapeInfo);
		final Collection<Triple<EnterMonitorStmt, ExitMonitorStmt, SootMethod>> _deadlockingMonitors = _dla
				.getDeadlockingMonitors();
		System.out.println("Deadlocking Monitors: " + _deadlockingMonitors.size() + " -- using escapeInfo :"
				+ (escapeInfo != null) + " -- using locking based equiv : " + (lbe != null));
		for (final Triple<EnterMonitorStmt, ExitMonitorStmt, SootMethod> _m : _deadlockingMonitors) {
			System.out.println("\t" + _m);
		}
	}
}
