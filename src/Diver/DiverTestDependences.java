package Diver;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ksu.cis.indus.common.collections.MapUtils;
import edu.ksu.cis.indus.common.collections.SetUtils;
import edu.ksu.cis.indus.common.datastructures.Pair.PairManager;
import edu.ksu.cis.indus.common.soot.BasicBlockGraphMgr;
import edu.ksu.cis.indus.common.soot.IStmtGraphFactory;
import edu.ksu.cis.indus.common.soot.MetricsProcessor;
import edu.ksu.cis.indus.common.soot.NamedTag;
import edu.ksu.cis.indus.common.soot.SootBasedDriver;
import edu.ksu.cis.indus.interfaces.ICallGraphInfo;
import edu.ksu.cis.indus.interfaces.IEnvironment;
import edu.ksu.cis.indus.interfaces.IEscapeInfo;
import edu.ksu.cis.indus.interfaces.IExceptionRaisingInfo;
import edu.ksu.cis.indus.interfaces.IMonitorInfo;
import edu.ksu.cis.indus.interfaces.IReadWriteInfo;
import edu.ksu.cis.indus.interfaces.IThreadGraphInfo;
import edu.ksu.cis.indus.interfaces.IUseDefInfo;
import edu.ksu.cis.indus.processing.IProcessor;
import edu.ksu.cis.indus.processing.OneAllStmtSequenceRetriever;
import edu.ksu.cis.indus.processing.ProcessingController;
import edu.ksu.cis.indus.processing.TagBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.callgraphs.CGBasedXMLizingProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.callgraphs.CallGraphInfo;
import edu.ksu.cis.indus.staticanalyses.callgraphs.OFABasedCallInfoCollector;
import edu.ksu.cis.indus.staticanalyses.cfg.CFGAnalysis;
import edu.ksu.cis.indus.staticanalyses.cfg.ExceptionRaisingAnalysis;
import edu.ksu.cis.indus.staticanalyses.cfg.StaticFieldUseDefInfo;
import edu.ksu.cis.indus.staticanalyses.concurrency.MonitorAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.SafeLockAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.escape.EquivalenceClassBasedEscapeAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.independence.IndependentRegionDetector;
import edu.ksu.cis.indus.staticanalyses.concurrency.independence.IndependentStmtDetector;
import edu.ksu.cis.indus.staticanalyses.concurrency.independence.IndependentStmtDetectorv2;
import edu.ksu.cis.indus.staticanalyses.dependency.DependencyXMLizer;
//import edu.ksu.cis.indus.staticanalyses.dependency.DependencyXMLizerCLI;
import edu.ksu.cis.indus.staticanalyses.dependency.DivergenceDA;
import edu.ksu.cis.indus.staticanalyses.dependency.ExitControlDA;
import edu.ksu.cis.indus.staticanalyses.dependency.IDependencyAnalysis;
import edu.ksu.cis.indus.staticanalyses.dependency.IdentifierBasedDataDA;
import edu.ksu.cis.indus.staticanalyses.dependency.IdentifierBasedDataDAv2;
import edu.ksu.cis.indus.staticanalyses.dependency.IdentifierBasedDataDAv3;
import edu.ksu.cis.indus.staticanalyses.dependency.InterProceduralDivergenceDA;
import edu.ksu.cis.indus.staticanalyses.dependency.InterferenceDAv1;
import edu.ksu.cis.indus.staticanalyses.dependency.InterferenceDAv2;
import edu.ksu.cis.indus.staticanalyses.dependency.InterferenceDAv3;
import edu.ksu.cis.indus.staticanalyses.dependency.NonTerminationInsensitiveEntryControlDA;
import edu.ksu.cis.indus.staticanalyses.dependency.NonTerminationSensitiveEntryControlDA;
import edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv1;
import edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv2;
import edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv3;
import edu.ksu.cis.indus.staticanalyses.dependency.ReferenceBasedDataDA;
import edu.ksu.cis.indus.staticanalyses.dependency.SynchronizationDA;
import edu.ksu.cis.indus.staticanalyses.dependency.SystemDependenceGraphBuilder;
import edu.ksu.cis.indus.staticanalyses.dependency.IDependencyAnalysis.DependenceSort;
import edu.ksu.cis.indus.staticanalyses.flow.instances.ofa.OFAnalyzer;
import edu.ksu.cis.indus.staticanalyses.flow.processors.AliasedUseDefInfo;
import edu.ksu.cis.indus.staticanalyses.flow.processors.AliasedUseDefInfov2;
import edu.ksu.cis.indus.staticanalyses.flow.processors.ThreadGraph;
import edu.ksu.cis.indus.staticanalyses.interfaces.IValueAnalyzer;
import edu.ksu.cis.indus.staticanalyses.processing.AnalysesController;
import edu.ksu.cis.indus.staticanalyses.processing.CGBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.processing.ValueAnalyzerBasedProcessingController;
import edu.ksu.cis.indus.staticanalyses.tokens.ITokens;
import edu.ksu.cis.indus.staticanalyses.tokens.TokenUtil;
import edu.ksu.cis.indus.staticanalyses.tokens.soot.SootValueTypeManager;
import edu.ksu.cis.indus.xmlizer.UniqueJimpleIDGenerator;
import soot.Body;
import soot.Local;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.jimple.FieldRef;
import soot.jimple.Stmt;
import soot.jimple.spark.SparkTransformer;
import soot.options.Options;

public class DiverTestDependences extends SootBasedDriver  {
	public static String path = "";
	public static String outPath = "";
	static boolean debugOut = false;
	/**
	 * The logger used by instances of this class to log messages.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(DiverTestDependences.class);

	/**
	 * This is the flow analyser used by the analyses being tested.
	 */
	protected IValueAnalyzer<Value> aa;

	/**
	 * A collection of dependence analyses.
	 */
	protected List<IDependencyAnalysis> das = new ArrayList<IDependencyAnalysis>();

	/**
	 * This is a map from interface IDs to interface implementations that are required by the analyses being driven.
	 * 
	 * @invariant info.oclIsKindOf(Map(String, Object))
	 */
	protected final Map info = new HashMap();

	/**
	 * This indicates if common unchecked exceptions should be considered.
	 */
	private boolean commonUncheckedException;

	/**
	 * This flag indicates if jimple should be dumped.
	 */
	private boolean dumpJimple;

	/**
	 * This indicates if exceptional exits should be considered.
	 */
	private boolean exceptionalExits;

	/**
	 * This flag indicates if the simple version of aliased use-def information should be used.
	 */
	private boolean useAliasedUseDefv1;

	/**
	 * This indicates if safe lock should be used.
	 */
	private boolean useSafeLockAnalysis;

	/**
	 * The xmlizer used to xmlize dependence information.
	 */
	private final DependencyXMLizer xmlizer = new DependencyXMLizer();
	public static void main(String args[]){
		if (args.length < 1) {
			System.err.println("Too few arguments: \n\t ");
			return;
		}
		path = args[0]; // 
		if (args.length > 1) {
			outPath = args[1];
		}
		else
		{
			outPath = path;
		}
		if (args.length > 2) {
			debugOut = args[2].equalsIgnoreCase("-debug");
		}
		initial(path);
		enableSpark(path);
		final DivergenceDA _fidda = DivergenceDA.getDivergenceDA(IDependencyAnalysis.Direction.FORWARD_DIRECTION);
		_fidda.setConsiderCallSites(true);

		final DivergenceDA _bidda = DivergenceDA.getDivergenceDA(IDependencyAnalysis.Direction.BACKWARD_DIRECTION);
		_bidda.setConsiderCallSites(true);

		final NonTerminationSensitiveEntryControlDA _ncda = new NonTerminationSensitiveEntryControlDA();
		final Object[][] _dasOptions = {
				{"sda", "Synchronization dependence", new SynchronizationDA()},
				{"frda1", "Forward Ready dependence v1", ReadyDAv1.getForwardReadyDA()},
				{"brda1", "Backward Ready dependence v1", ReadyDAv1.getBackwardReadyDA()},
				{"frda2", "Forward Ready dependence v2", ReadyDAv2.getForwardReadyDA()},
				{"brda2", "Backward Ready dependence v2", ReadyDAv2.getBackwardReadyDA()},
				{"frda3", "Forward Ready dependence v3", ReadyDAv3.getForwardReadyDA()},
				{"brda3", "Backward Ready dependence v3", ReadyDAv3.getBackwardReadyDA()},
				{"ida1", "Interference dependence v1", new InterferenceDAv1()},
				{"ida2", "Interference dependence v2", new InterferenceDAv2()},
				{"ida3", "Interference dependence v3", new InterferenceDAv3()},
				};
		for (int _i = 0; _i < _dasOptions.length; _i++) {
			final String _shortOption = _dasOptions[_i][0].toString();
			final String _description = _dasOptions[_i][1].toString();
			//_option = new Option(_shortOption, false, _description);
			//_options.addOption(_option);
		}
		Collection<String> classes= new ArrayList<String>();
		/* traverse all application classes */
		for (SootClass sClass:Scene.v().getApplicationClasses()) 
		{
			classes.add(sClass.toString());
		}
		if (debugOut)
			System.out.println("classes="+classes);	
		DiverTestDependences _xmlizerCLI = new DiverTestDependences();
		_xmlizerCLI.xmlizer.setXmlOutputDir(outPath);
		_xmlizerCLI.dumpJimple = false;  // _cl.hasOption('j');
		_xmlizerCLI.useAliasedUseDefv1 = false;    //_cl.hasOption("aliasedusedefv1");
		_xmlizerCLI.useSafeLockAnalysis = false;    //_cl.hasOption("safelockanalysis");
		_xmlizerCLI.exceptionalExits = false;    //_cl.hasOption("exceptionalexits");
		_xmlizerCLI.commonUncheckedException = false;    //_cl.hasOption("commonuncheckedexceptions");
		_xmlizerCLI.das.add(_ncda);
		for (final Iterator<DependenceSort> _i = _ncda.getIds().iterator(); _i.hasNext();) {
			final DependenceSort _id = _i.next();
			MapUtils.putIntoCollectionInMapUsingFactory(_xmlizerCLI.info, _id, _ncda, SetUtils.getFactory());
		}
		if (!parseForDependenceOptions(_dasOptions, _xmlizerCLI)) {
			System.out.println("Atleast one dependence analysis must be requested.");
		}
		if (classes.isEmpty()) {
			System.out.println("Please specify at least one class.");
		}
		_xmlizerCLI.setClassNames(classes);			
		_xmlizerCLI.<ITokens> execute();
	}	

	
	// soot option 1
	private static void initial(String classPath) {
		soot.G.reset();
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_process_dir(Collections.singletonList(classPath));//
		Options.v().set_whole_program(true);
		Scene.v().loadNecessaryClasses();
		
	}
	
	// soot option 2
    private static void enableSpark(String path){
        HashMap opt = new HashMap();
        //opt.put("verbose","true");
        //opt.put("propagator","worklist");
        opt.put("simple-edges-bidirectional","false");
        //opt.put("on-fly-cg","true");
        opt.put("apponly", "true");
//        opt.put("set-impl","double");
//        opt.put("double-set-old","hybrid");
//        opt.put("double-set-new","hybrid");
//        opt.put("allow-phantom-refs", "true");
        opt.put("-process-dir",path);
        
        SparkTransformer.v().transform("",opt);
    }

	/**
	 * Drives the analyses.
	 * 
	 * @param <T> dummy type parameter.
	 */
	private <T extends ITokens<T, Value>> void execute() {
		setInfoLogger(LOGGER);

		final String _tagName = "DependencyXMLizer:FA";
		aa = OFAnalyzer.getFSOSAnalyzer(_tagName, TokenUtil.<T, Value, Type> getTokenManager(new SootValueTypeManager()),
				getStmtGraphFactory());

		final ValueAnalyzerBasedProcessingController _pc = new ValueAnalyzerBasedProcessingController();
		final Collection<IProcessor> _processors = new ArrayList<IProcessor>();
		final PairManager _pairManager = new PairManager(false, true);
		final CallGraphInfo _cgi = new CallGraphInfo(new PairManager(false, true));
		final IThreadGraphInfo _tgi = new ThreadGraph(_cgi, new CFGAnalysis(_cgi, getBbm()), _pairManager);
		final IExceptionRaisingInfo _eti = new ExceptionRaisingAnalysis(getStmtGraphFactory(), _cgi, aa.getEnvironment());
		final ProcessingController _xmlcgipc = new ProcessingController();
		final ValueAnalyzerBasedProcessingController _cgipc = new ValueAnalyzerBasedProcessingController();
		final MetricsProcessor _countingProcessor = new MetricsProcessor();
		final OFABasedCallInfoCollector _callGraphInfoCollector = new OFABasedCallInfoCollector();
		final OneAllStmtSequenceRetriever _ssr = new OneAllStmtSequenceRetriever();
		_ssr.setStmtGraphFactory(getStmtGraphFactory());
		_pc.setStmtSequencesRetriever(_ssr);
		_pc.setAnalyzer(aa);
		_pc.setProcessingFilter(new TagBasedProcessingFilter(_tagName));

		_cgipc.setAnalyzer(aa);
		_cgipc.setProcessingFilter(new CGBasedProcessingFilter(_cgi));
		_cgipc.setStmtSequencesRetriever(_ssr);

		_xmlcgipc.setEnvironment(aa.getEnvironment());
		_xmlcgipc.setProcessingFilter(new CGBasedXMLizingProcessingFilter(_cgi));
		_xmlcgipc.setStmtSequencesRetriever(_ssr);

		final StaticFieldUseDefInfo _staticFieldUD = new StaticFieldUseDefInfo();
		final AliasedUseDefInfo _aliasUD;

		if (useAliasedUseDefv1) {
			_aliasUD = new AliasedUseDefInfo(aa, bbm, _pairManager, new CFGAnalysis(_cgi, bbm));
		} else {
			_aliasUD = new AliasedUseDefInfov2(aa, _cgi, _tgi, bbm, _pairManager);
		}
		info.put(ICallGraphInfo.ID, _cgi);
		info.put(IThreadGraphInfo.ID, _tgi);
		info.put(PairManager.ID, _pairManager);
		info.put(IEnvironment.ID, aa.getEnvironment());
		info.put(IValueAnalyzer.ID, aa);
		info.put(IUseDefInfo.ALIASED_USE_DEF_ID, _aliasUD);
		info.put(IUseDefInfo.GLOBAL_USE_DEF_ID, _staticFieldUD);
		info.put(IStmtGraphFactory.ID, getStmtGraphFactory());

		final EquivalenceClassBasedEscapeAnalysis _ecba = new EquivalenceClassBasedEscapeAnalysis(_cgi, _tgi, getBbm());
		info.put(IEscapeInfo.ID, _ecba.getEscapeInfo());

		final IMonitorInfo _monitorInfo = new MonitorAnalysis();
		info.put(IMonitorInfo.ID, _monitorInfo);

		final SafeLockAnalysis _sla;

		if (useSafeLockAnalysis) {
			_sla = new SafeLockAnalysis();
			info.put(SafeLockAnalysis.ID, _sla);
		} else {
			_sla = null;
		}

		initialize();
		aa.analyze(getEnvironment(), getRootMethods());

		_callGraphInfoCollector.reset();
		_processors.clear();
		_processors.add(_callGraphInfoCollector);
		_pc.reset();
		_pc.driveProcessors(_processors);
		_cgi.createCallGraphInfo(_callGraphInfoCollector.getCallInfo());
		writeInfo("CALL GRAPH:\n" + _cgi.toString());

		if (commonUncheckedException) {
			final ExceptionRaisingAnalysis _t = (ExceptionRaisingAnalysis) _eti;
			_t.setupForCommonUncheckedExceptions();
		}

		_processors.clear();
		((ThreadGraph) _tgi).reset();
		_processors.add((IProcessor) _tgi);
		_processors.add((IProcessor) _eti);
		_processors.add(_countingProcessor);
		_cgipc.reset();
		_cgipc.driveProcessors(_processors);
		writeInfo("THREAD GRAPH:\n" + ((ThreadGraph) _tgi).toString());
		//writeInfo("EXCEPTION THROW INFO:\n" + ((ExceptionRaisingAnalysis) _eti).toString());
		writeInfo("STATISTICS: " + MapUtils.verbosePrint(new TreeMap(_countingProcessor.getStatistics())));

		_aliasUD.hookup(_cgipc);
		_staticFieldUD.hookup(_cgipc);
		_cgipc.process();
		_staticFieldUD.unhook(_cgipc);
		_aliasUD.unhook(_cgipc);

		writeInfo("BEGIN: dependency analyses");

		if (exceptionalExits) {
			bbm = new BasicBlockGraphMgr(_eti);
			bbm.setStmtGraphFactory(getStmtGraphFactory());
		}

		final AnalysesController _ac = new AnalysesController(info, _cgipc, getBbm());
		_ac.addAnalyses(IMonitorInfo.ID, Collections.singleton((MonitorAnalysis) _monitorInfo));
		_ac.addAnalyses(EquivalenceClassBasedEscapeAnalysis.ID, Collections.singleton(_ecba));

		if (useSafeLockAnalysis) {
			_ac.addAnalyses(SafeLockAnalysis.ID, Collections.singleton(_sla));
		}

		for (final Iterator _i1 = das.iterator(); _i1.hasNext();) {
			final IDependencyAnalysis _da1 = (IDependencyAnalysis) _i1.next();

			for (final Iterator<? extends Comparable<? extends Object>> _i2 = _da1.getIds().iterator(); _i2.hasNext();) {
				final Comparable<? extends Object> _id = _i2.next();
				_ac.addAnalyses(_id, Collections.singleton(_da1));
			}
		}

		_ac.initialize();
		_ac.execute();

		// write xml
		for (final Iterator _i1 = das.iterator(); _i1.hasNext();) {
			final IDependencyAnalysis _da1 = (IDependencyAnalysis) _i1.next();

			for (final Iterator _i2 = _da1.getIds().iterator(); _i2.hasNext();) {
				final Object _id = _i2.next();
				MapUtils.putIntoListInMap(info, _id, _da1);
			}
		}
		xmlizer.setGenerator(new UniqueJimpleIDGenerator());
		//System.out.println("info="+info.toString());
		xmlizer.writeXML(info);

		if (dumpJimple) {
			xmlizer.dumpJimple(null, xmlizer.getXmlOutputDir(), _xmlcgipc);
		}
		writeInfo("Total classes loaded: " + getEnvironment().getClasses().size());
		
		SystemDependenceGraphBuilder.getSystemDependenceGraph(das, _cgi, getEnvironment().getClasses());
	}
	
	/**
	 * Parses command line for dependence analysis options.
	 * 
	 * @param dependenceOptions supported by this CLI.
	 * @param cmdLine provided by the user.
	 * @param xmlizerCLI that will be influenced by the provided dependence analysis options.
	 * @return <code>false</code> if no dependence options were parsed; <code>true</code>, otherwise.
	 */
	private static boolean parseForDependenceOptions(final Object[][] dependenceOptions, final DiverTestDependences xmlizerCLI) {
		boolean _flag = false;

		for (int _i = 0; _i < dependenceOptions.length; _i++) {
			{
				final IDependencyAnalysis _da = (IDependencyAnalysis) dependenceOptions[_i][2];
				xmlizerCLI.das.add(_da);
				_flag = true;

				if (_da instanceof InterferenceDAv1) {
					((InterferenceDAv1) _da).setUseOFA(false);
				}

				if (_da instanceof ReadyDAv1) {
					((ReadyDAv1) _da).setUseOFA(false);
					((ReadyDAv1) _da).setUseSafeLockAnalysis(false);
				}
			}
		}
		return _flag;
	}

}

// End of File