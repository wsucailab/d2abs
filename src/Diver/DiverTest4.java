package Diver;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
//import org.apache.commons.cli.Option;
//import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import soot.options.Options;

import edu.ksu.cis.indus.common.collections.IteratorUtils;
import edu.ksu.cis.indus.common.collections.MapUtils;
import edu.ksu.cis.indus.common.soot.Constants;
import edu.ksu.cis.indus.common.soot.SootBasedDriver;
import edu.ksu.cis.indus.common.soot.SootPredicatesAndTransformers;
import edu.ksu.cis.indus.common.soot.Util;
import soot.jimple.spark.SparkTransformer;

import soot.*;
import soot.jimple.*;
import edu.ksu.cis.indus.annotations.NonNull;
import edu.ksu.cis.indus.annotations.NonNullContainer;
import edu.ksu.cis.indus.common.collections.IteratorUtils;
import edu.ksu.cis.indus.common.datastructures.HistoryAwareFIFOWorkBag;
import edu.ksu.cis.indus.common.datastructures.IWorkBag;
import edu.ksu.cis.indus.common.datastructures.Triple;
import edu.ksu.cis.indus.common.datastructures.Pair.PairManager;
import edu.ksu.cis.indus.common.soot.SootPredicatesAndTransformers;
import edu.ksu.cis.indus.interfaces.ICallGraphInfo;
import edu.ksu.cis.indus.interfaces.IEnvironment;
import edu.ksu.cis.indus.interfaces.IThreadGraphInfo;
import edu.ksu.cis.indus.interfaces.ICallGraphInfo.CallTriple;
import edu.ksu.cis.indus.processing.Context;
import edu.ksu.cis.indus.processing.IProcessor;
import edu.ksu.cis.indus.processing.OneAllStmtSequenceRetriever;
import edu.ksu.cis.indus.processing.TagBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.callgraphs.CallGraphInfo;
import edu.ksu.cis.indus.staticanalyses.callgraphs.OFABasedCallInfoCollector;
import edu.ksu.cis.indus.staticanalyses.cfg.CFGAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.MonitorAnalysis;
import edu.ksu.cis.indus.staticanalyses.flow.instances.ofa.OFAnalyzer;
import edu.ksu.cis.indus.staticanalyses.flow.processors.ThreadGraph;
import edu.ksu.cis.indus.staticanalyses.interfaces.IValueAnalyzer;
import edu.ksu.cis.indus.staticanalyses.processing.CGBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.processing.ValueAnalyzerBasedProcessingController;
import edu.ksu.cis.indus.staticanalyses.tokens.ITokens;
import edu.ksu.cis.indus.staticanalyses.tokens.TokenUtil;
import edu.ksu.cis.indus.staticanalyses.tokens.soot.SootValueTypeManager;
public class DiverTest4  extends SootBasedDriver {
	public static String path = "C:/Soot/In"; //"C:/Research/nioecho/bin";  ///voldemort/rest/coordinator/admin
	static boolean debugOut = true;
	private static Logger LOGGER = LoggerFactory.getLogger(DiverTest4.class);
//	private Collection<SootMethod> threadEntryPoints = new HashSet<SootMethod>();
//	private Map<Triple<InvokeStmt, SootMethod, SootClass>, Collection<SootMethod>> thread2methods = new HashMap<Triple<InvokeStmt, SootMethod, SootClass>, Collection<SootMethod>>();
//	private Map<SootMethod, Collection<Triple<InvokeStmt, SootMethod, SootClass>>> method2threadCreationSite = new HashMap<SootMethod, Collection<Triple<InvokeStmt, SootMethod, SootClass>>>(
//			Constants.getNumOfMethodsInApplication());
	/**
	 * This provides object flow information required by this analysis.
	 */
	private static IValueAnalyzer<Value> analyzer;

	/**
	 * This provides call graph information pertaining to the system.
	 * 
	 * @invariant cgi != null
	 */
//	private ICallGraphInfo cgi;
	public static void main(String args[]) {
		
//      if(args.length == 0)
//      {
//          System.out.println("Usage: dtSourceSink directory");
//          System.exit(0);
//      }            
//      else
//			System.out.println("[mainClass]"+args[0]);	
//      path = args[0]; 
		
	
		initial(path);
		enableSpark(path);
//		SootClass _sc=Scene.v().loadClassAndSupport("C:\\Soot\\In\\Test0.class");
//		System.out.println("_sc="+_sc);
//		_sc.setApplicationClass();
//		for (SootClass sClass:Scene.v().getApplicationClasses()) 
//		{
//			System.out.println("getApplicationClasses()="+sClass);
//////			SootMethod _runMethod = sClass.getMethodByName("run"); 
//////			if (_runMethod!=null)
//////				System.out.println(" _startMethod="+_runMethod);
////			if ( sClass.isPhantom() ) {	continue; }
////			if ( !sClass.isConcrete() ) {	continue; }
//			
//			for(SootMethod m:sClass.getMethods())
//			{
//				System.out.println("m="+m);
//				Iterator<Unit> units=m.retrieveActiveBody().getUnits().snapshotIterator();
//				//System.out.println("units="+units);
//				while(units.hasNext())
//				{
//					Unit u=units.next();
//					System.out.println("u= "+u);
//				}	
//				
////				Body b=m.retrieveActiveBody();
////				System.out.println("[body]="+b);
//				/*
//				Iterator<Unit> units=m.retrieveActiveBody().getUnits().snapshotIterator();
//				while(units.hasNext())
//				{
//					Unit u=units.next();
//					System.out.println("u="+u);
//				
//				}
//////				*/
//			}	
//		
//		}
//		IEnvironment _env = analyzer.getEnvironment();
//		SootClass _threadClass = _env.getClass("java.lang.Thread");
//		SootMethod _startMethod = _threadClass.getMethodByName("start"); 

//		org.apache.commons.cli.Options _options = new org.apache.commons.cli.Options();
//		
//		org.apache.commons.cli.Option _option = new org.apache.commons.cli.Option("h", "help", false, "Display message.");
//		_option.setOptionalArg(false);
//		_options.addOption(_option);
//		_option = new org.apache.commons.cli.Option("p", "soot-classpath", false, "Prepend this to soot class path.");
//		_option.setArgs(1);
//		_option.setArgName("classpath");
//		_option.setOptionalArg(false);
//		_options.addOption(_option);
//		_option = new org.apache.commons.cli.Option("S", "scope", true, "The scope that should be analyzed.");
//		_option.setArgs(1);
//		_option.setArgName("scope");
//		_option.setRequired(false);
//		_options.addOption(_option);
//
//		CommandLineParser _parser = new GnuParser();
		
		try {
//			CommandLine _cl = _parser.parse(_options, args);
//
//			if (_cl.hasOption("h")) {
//				String _cmdLineSyn = "java " + DiverTest4.class.getName() + " <options> <classnames>";
//				(new HelpFormatter()).printHelp(_cmdLineSyn, _options);
//				System.exit(1);
//			}
//
//			if (_cl.getArgList().isEmpty()) {
//				throw new MissingArgumentException("Please specify atleast one class.");
//			}

			DiverTest4 _cli = new DiverTest4();

//			if (_cl.hasOption('p')) {
//				_cli.addToSootClassPath(_cl.getOptionValue('p'));
//			}
//
//			if (_cl.hasOption('S')) {
//				_cli.setScopeSpecFile(_cl.getOptionValue('S'));
//			}
//			System.out.println("_cl.getArgList()="+_cl.getArgList());
			Collection<String> classes= new ArrayList<String>();
//			classes.add("ThreadMain");
//			classes.add("Ticket");
			/* traverse all classes */
			Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
			for (SootClass sClass:Scene.v().getApplicationClasses()) 
			{
				classes.add(sClass.toString());
			}	
			System.out.println("classes="+classes);	
			_cli.setClassNames(classes);			
			_cli.<ITokens> execute();
			
		} 
//		catch (ParseException _e) {
//			LOGGER.error("Error while parsing command line.", _e);
//			System.out.println("Error while parsing command line." + _e);
//			String _cmdLineSyn = "java " + DiverTest4.class.getName() + " <options> <classnames>";
//			(new HelpFormatter()).printHelp(_cmdLineSyn, "Options are:", _options, "");
//		} 
	catch (Throwable _e) {
			LOGGER.error("Beyond our control. May day! May day!", _e);
			throw new RuntimeException(_e);
		}
		
	}
 
	/**
	 * This contains the driver logic.
	 * 
	 * @param <T> dummy type parameter.
	 */
	private <T extends ITokens<T, Value>> void execute() {
		setInfoLogger(LOGGER);

		String _tagName = "SideEffect:FA";
		IValueAnalyzer<Value> _aa = OFAnalyzer.getFSOSAnalyzer(_tagName, TokenUtil
				.<T, Value, Type> getTokenManager(new SootValueTypeManager()), getStmtGraphFactory());
		ValueAnalyzerBasedProcessingController _pc = new ValueAnalyzerBasedProcessingController();
		Collection<IProcessor> _processors = new ArrayList<IProcessor>();
		PairManager _pairManager = new PairManager(false, true);
		CallGraphInfo _cgi = new CallGraphInfo(new PairManager(false, true));
		MonitorAnalysis _monitorInfo = new MonitorAnalysis();
		OFABasedCallInfoCollector _callGraphInfoCollector = new OFABasedCallInfoCollector();
		IThreadGraphInfo _tgi = new ThreadGraph(_cgi, new CFGAnalysis(_cgi, getBbm()), _pairManager);
		ValueAnalyzerBasedProcessingController _cgipc = new ValueAnalyzerBasedProcessingController();
		OneAllStmtSequenceRetriever _ssr = new OneAllStmtSequenceRetriever();

		_ssr.setStmtGraphFactory(getStmtGraphFactory());

		_pc.setStmtSequencesRetriever(_ssr);
		_pc.setAnalyzer(_aa);
		_pc.setProcessingFilter(new TagBasedProcessingFilter(_tagName));

		_cgipc.setAnalyzer(_aa);
		_cgipc.setProcessingFilter(new CGBasedProcessingFilter(_cgi));
		_cgipc.setStmtSequencesRetriever(_ssr);

		Map _info = new HashMap();
		_info.put(ICallGraphInfo.ID, _cgi);
		_info.put(IThreadGraphInfo.ID, _tgi);
		_info.put(PairManager.ID, _pairManager);
		_info.put(IEnvironment.ID, _aa.getEnvironment());
		_info.put(IValueAnalyzer.ID, _aa);

		initialize();
		_aa.analyze(getEnvironment(), getRootMethods());

		_processors.clear();
		_processors.add(_callGraphInfoCollector);
		_pc.reset();
		_pc.driveProcessors(_processors);
		_cgi.reset();
		_cgi.createCallGraphInfo(_callGraphInfoCollector.getCallInfo());
		writeInfo("CALL GRAPH:\n" + _cgi.toString());
		System.out.println("CALL GRAPH:\n" + _cgi.toString());
		
		_processors.clear();
		((ThreadGraph) _tgi).reset();
		_processors.add((IProcessor) _tgi);
		_cgipc.reset();
		_cgipc.driveProcessors(_processors);
		writeInfo("THREAD GRAPH:\n" + ((ThreadGraph) _tgi).toString());
		System.out.println("THREAD GRAPH:\n" + ((ThreadGraph) _tgi).toString());
	}
	
	// soot option 1
	private static void initial(String classPath) {
		soot.G.reset();
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_process_dir(Collections.singletonList(classPath));//
		Options.v().set_whole_program(true);
		Scene.v().loadNecessaryClasses();
		
	}
	
	// soot option 2
    private static void enableSpark(String path){
        HashMap opt = new HashMap();
        //opt.put("verbose","true");
        //opt.put("propagator","worklist");
        opt.put("simple-edges-bidirectional","false");
        //opt.put("on-fly-cg","true");
        opt.put("apponly", "true");
//        opt.put("set-impl","double");
//        opt.put("double-set-old","hybrid");
//        opt.put("double-set-new","hybrid");
//        opt.put("allow-phantom-refs", "true");
        opt.put("-process-dir",path);
        
        SparkTransformer.v().transform("",opt);
    }



}