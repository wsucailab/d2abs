package Diver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ThreadUtil {
	public static void main(String []args) {
		//System.out.println("isEqualThreadStmt(String stmt1, String stmt2)="+isEqualThreadStmt("r2 = $r25;", "$r25 = new java.lang.Thread")); 
//		ArrayList<String> threadMethodStmts = getThreadMethodStmts("C:/Research/Indus/nioEchoTest7.log");
//		System.out.println(" threadInvokeStmts="+threadMethodStmts);
////		System.out.println(" getMethodsFromArray(threadInvokeStmts)="+getMethodsFromArray(threadInvokeStmts));
////		System.out.println(" getStmtsFromArray(threadInvokeStmts)="+getStmtsFromArray(threadInvokeStmts));
////		System.out.println(" getStmtIndexFromArray(ArrayList<String> threads, String myStmt)="+getStmtIndexFromArray(threadInvokeStmts,"virtualinvoke r5.<"));
//		//
//		System.out.println(" getThreadReadyDAvDependence="+getThreadReadyDAvDependence("C:/Research/Indus/nioEchoTest7.log", "READY_DA info Value[5]=Statistics for Ready dependence as calculated by edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv3","A total of ",threadMethodStmts,"C:/Research/Indus/nioEchoTest2.log"));
		//System.out.println(" getAllDependence="+getAllDependence("C:/Research/Indus/nioEchoTest7.log","C:/Research/Indus/nioEchoTest2.log"));
//		HashSet<String> array1=getAllDependence("C:/Research/Indus/DiverTest7.log","C:/Research/Indus/DiverTest2.log");
//		writeHashSetToFileWithType(array1, "C:/Research/Indus/ProducerConsumerAllDependence.log", "");
//		
		HashMap<Integer, HashSet<String>> hses=getDeDtFromFile("C:/Research/Indus/ProducerConsumerThreadDependence.log");
		HashSet<String> hs1=hses.get(0);
		HashSet<String> hs2=hses.get(1);
		System.out.println(" des="+hs1);
		System.out.println(" dts="+hs2);
		
	}
	public static boolean isEqualThreadStmt(String stmt1, String stmt2) {  
        //boolean result=false;
        if (stmt1.indexOf(" new java.")<0 || stmt2.indexOf(" new java.")<0 || stmt1.indexOf(" = ")<0 || stmt2.indexOf(" = ")<0)
        	return stmt1.equals(stmt2);
        String[] stmt1s=stmt1.split(" new java.");
        String[] stmt2s=stmt2.split(" new java.");
        return stmt1s[0].equals(stmt2s[0]);
       
	}
	public static ArrayList<String> getThreadInvokeStmts(String listFile) {
		ArrayList  resultA = new ArrayList<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;     
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = null;  
            readLinesUntil(br, "THREAD GRAPH:");
            while ((str = br.readLine()) != null) {  
            	if (str.startsWith("First: "))
            		resultA.add(str.replace("First: ", ""));   
            	if (str.indexOf("Method-Thread mapping:")>=0)
            		return resultA; 
            }     
            br.close();  
            reader.close();     
            return resultA;   
        } catch (Exception e) {  
            e.printStackTrace();   
            return resultA;    
        }  
	}
	
	public static ArrayList<String> getThreadMethodStmts(String listFile) {
		ArrayList  resultA = new ArrayList<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
        String strFirst="";
        String strSecond="";
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = null;  
            readLinesUntil(br, "THREAD GRAPH:");
            while ((str = br.readLine()) != null) {  
            	if (str.startsWith("First: "))  {
            		strFirst=str.replace("First: ", "");
            		strSecond="";
            		str = br.readLine();
            		if (str!=null && str.startsWith("Second: ")) {
            			strSecond=str.replace("Second: ", "");
            			resultA.add(strSecond+" - "+strFirst);
            		}
            		else
            		{
            	        strFirst="";
            	        strSecond="";
            		}
            	}   
            	if (str.indexOf("Method-Thread mapping:")>=0)
            		return resultA; 
            }     
            br.close();  
            reader.close();     
            return resultA;   
        } catch (Exception e) {  
            e.printStackTrace();   
            return resultA;    
        }  
	}
	
	public static ArrayList<String> getJimpleMethodStmts(String listFile) {
		ArrayList  resultA = new ArrayList<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
        String strFirst="";
        String strSecond="";
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = null;  
            while ((str = br.readLine()) != null) {  
            	if (str.startsWith("<") && str.indexOf("> - ")>1)  
            	{
            		resultA.add(str);            		
            	}   
            	
            }     
            br.close();  
            reader.close();     
            return resultA;   
        } catch (Exception e) {  
            e.printStackTrace();   
            return resultA;    
        }  
	}
	public static ArrayList<String> getMethodsFromArray(ArrayList<String> threads) {
		ArrayList  resultA = new ArrayList<String>(); 
		String str="";
		for (int i=0; i<threads.size();i++)
		{
			str=threads.get(i);
			String strs[]=str.split("> - ");
			if (strs.length<1)
				continue;
			resultA.add(strs[0]+">");
		}	
		return resultA;  
	}
	
	public static int getStmtIndexFromArray(ArrayList<String> threads, String myStmt) {
		int resultI=-1;
		String str="";
		String strStmt="";
		for (int i=0; i<threads.size();i++)
		{
			str=threads.get(i);
			//System.out.println("str="+str);
			if (str.equals(myStmt)) 
				return i;
			String strs[]=str.split("> - ");
			if (strs.length<2)
				continue;
			strStmt=strs[1];
			//System.out.println("strStmt="+strStmt);
			if (strStmt.equals(myStmt)) 
				return i;
			String strStmts[]=strStmt.split(".<");
			if (strStmts.length<1)
				continue;
			//System.out.println("strStmts[0]="+strStmts[0]);
			if ((strStmts[0].equals(myStmt)) || ((strStmts[0]+".<").equals(myStmt)) || (strStmts[0].equals(myStmt+".<")))
				return i;
		}	
		return resultI;  
	}
	
	public static ArrayList<String> getStmtsFromArray(ArrayList<String> threads) {
		ArrayList  resultA = new ArrayList<String>(); 
		String str="";
		String strStmt="";
		for (int i=0; i<threads.size();i++)
		{
			str=threads.get(i);
			String strs[]=str.split("> - ");
			if (strs.length<2)
				continue;
			strStmt=strs[1];
			String strStmts[]=strStmt.split(".<");
			if (strStmts.length<1)
				continue;
			resultA.add(strStmts[0]+".<");
		}	
		return resultA;  
	}
	public static HashSet<String> getThreadReadyDADependence(String listFile, String startS, String endS, ArrayList<String> threads,String jimpleFile) {
		HashSet<String>  resultA = new HashSet<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
        String InMethod="";
        String ForStmt="";
        String midStr="";
        String directionStr="";
        String leftStr="";
        String rightStr="";
        String itemStr="";
        String leftItem="";
        String rightItem="";
        String leftMethod="";
        String rightMethod="";
        String leftStmt="";
        String rightStmt="";
//        String threadMethod="";
//        String threadStr="";
        ArrayList<String> stmts=getStmtsFromArray(threads);
        //System.out.println("stmts="+stmts);
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = "";  
            readLinesUntil(br, startS);
            while ((str = br.readLine()) != null) {  
        		//System.out.println("str="+str);
            	if (str.startsWith("In method "))  {
            		InMethod=str.replace("In method ", "");
            		ForStmt="";        
            		//System.out.println("str="+str);
            	}   
            	else if (str.startsWith(" 	For "))  {
            		midStr=str.replace(" 	For ", "");         
            		String midStrs[]=midStr.split(" there ");
            		if (midStrs.length<1)
            		{
            			ForStmt=midStr;
            		}
            		else
            			ForStmt=midStrs[0];  
            		//System.out.println("str="+str);
            	}   
            	else if ((str.indexOf(" <-- ")>1 || str.indexOf(" --> ")>1) && InMethod.length()>0 && ForStmt.length()>0)	
    			{
            		if (str.indexOf(" <-- ")>1)
            		{	
            			directionStr=" <-- ";
            		}			
            		else
            			directionStr=" --> ";
            		itemStr=getArrayItemInLine(str, stmts);
            		if (itemStr.length()>0)
            		{
            			//System.out.println("str="+str+" itemStr="+itemStr);
            			String strs[]=str.split(directionStr);
            			leftStr=strs[0].trim();
            			rightStr=strs[1].trim();
            			//resultA.add(str);
            			//System.out.println("leftStr="+leftStr+" rightStr="+rightStr);
            			if (leftStr.indexOf(itemStr)>0)
						{
							leftItem=getDiverMethodStmt(jimpleFile, itemStr);
							rightItem=getDiverMethodStmt(jimpleFile,InMethod)+" - "+getDiverMethodStmt(jimpleFile,ForStmt);
						}
            			else if (rightStr.indexOf(itemStr)>0)
						{
							rightItem=getDiverMethodStmt(jimpleFile, itemStr);
							leftItem=getDiverMethodStmt(jimpleFile,InMethod)+" - "+getDiverMethodStmt(jimpleFile,ForStmt);
						}
            			//System.out.println("leftItem="+leftItem+" rightItem="+rightItem);
            			if (leftItem.length()>0 && rightItem.length()>0) 
            				resultA.add(leftItem+directionStr+rightItem);
            		}    
    			}
            	if (str.indexOf(endS)>=0)
            		return resultA; 
            }     
            br.close();  
            reader.close();  
        } catch (Exception e) {  
            e.printStackTrace();   
                
        }  
        return resultA;
	}

	public static ArrayList<String> getThreadINTERFERENCEDADependence(String listFile, String startS, String endS, ArrayList<String> threads,String jimpleFile) {
		ArrayList  resultA = new ArrayList<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
        String ForMethod="";
        //String ForStmt2="";
        String midStr="";
        String directionStr="";
        String leftStr="";
        String rightStr="";
        String itemStr="";
        String leftItem="";
        String rightItem="";
        String leftMethod="";
        String leftStmt="";
        String rightMethod="";
        String rightStmt="";
//        String threadMethod="";
//        String threadStr="";
        ArrayList<String> stmts=getStmtsFromArray(threads);
        //System.out.println("stmts="+stmts);
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = "";  
            readLinesUntil(br, startS);
            while ((str = br.readLine()) != null) {  
        		//System.out.println("str="+str);
            	if (str.startsWith(" 	For "))  {
            		midStr=str.replace(" 	For ", "");         
            		String midStrs[]=midStr.split(" there ");
            		if (midStrs.length<1)
            		{
            			ForMethod=midStr;
            		}
            		else
            			ForMethod=midStrs[0];  
            		//System.out.println("str="+str);
            	}   
            	else if ((str.indexOf(" <-- ")>1 || str.indexOf(" --> ")>1) && ForMethod.length()>0)	
    			{
            		if (str.indexOf(" <-- ")>1)
            		{	
            			directionStr=" <-- ";
            		}			
            		else
            			directionStr=" --> ";
            		itemStr=getArrayItemInLine(str, stmts);
            		if (itemStr.length()>0)
            		{
            			System.out.println("str="+str+" itemStr="+itemStr);
            			String strs[]=str.split(directionStr);
            			leftStr=strs[0].trim();
            			rightStr=strs[1].trim();
            			//resultA.add(str);
            			System.out.println("leftStr="+leftStr+" rightStr="+rightStr);
            			if (leftStr.indexOf(itemStr)>0)
						{
							leftItem=getDiverMethodStmt(jimpleFile, itemStr);
							rightItem=getDiverMethodStmt(jimpleFile,ForMethod)+" - "+getDiverMethodStmt(jimpleFile,itemStr);
						}
            			else if (rightStr.indexOf(itemStr)>0)
						{
							rightItem=getDiverMethodStmt(jimpleFile, itemStr);
							leftItem=getDiverMethodStmt(jimpleFile,ForMethod)+" - "+getDiverMethodStmt(jimpleFile,itemStr);
						}
            			System.out.println("leftItem="+leftItem+" rightItem="+rightItem);
            			if (leftItem.length()>0 && rightItem.length()>0) 
            				resultA.add(leftItem+directionStr+rightItem);
            		}            		  
            		
    			}
            	
            	
            	if (str.indexOf(endS)>=0)
            		return resultA; 
            }     
            br.close();  
            reader.close();  
        } catch (Exception e) {  
            e.printStackTrace();   
                
        }  
        return resultA;
	}
	public static String getDiverMethodStmt(String jimpleFile, String inStr) {
		String  resultS = inStr; 
        FileReader reader = null;  
        BufferedReader br = null;     
        try {  
   
            reader = new FileReader(jimpleFile);  
            br = new BufferedReader(reader);              
            String str = null;  
            while ((str = br.readLine()) != null) {   
            	if (str.indexOf(inStr)>=0)
            		return str; 
            }     
            br.close();  
            reader.close(); 
        } catch (Exception e) {  
            e.printStackTrace(); 
        }  
        return resultS; 
	}
	
	public static boolean isArrayInLine(String lineStr, ArrayList array1) {
		boolean  resultB = false; 
        String str="";
        for (int i=0; i<array1.size();i++)
		{
			str=(String) array1.get(i);
			if (lineStr.indexOf(str)>=0)
				return true;
		}	
        return resultB; 
	}
	public static String getArrayItemInLine(String lineStr, ArrayList array1) {
		String  resultS = ""; 
        String str="";
        for (int i=0; i<array1.size();i++)
		{
			str=(String) array1.get(i);
			if (lineStr.indexOf(str)>=0)
				return str;
		}	
        return resultS; 
	}
	
    // only read line until
    public static String readLinesUntil(BufferedReader br,String untilStr) {

    	try
    	{
    		String str = br.readLine();
    		
    		//System.out.print("readLinesUntil "+str+"\n");
             while (str != null && str.indexOf(untilStr)<0) {             	
             	str = br.readLine();      
             }
            
             return str;
    	}
    	catch (IOException e) {          	
            e.printStackTrace();
            return "";
        }  
    }
    
    public static HashSet<String> getAllDependence(String threadFile, String jimpleFile) {
    	HashSet<String> allDependence= new HashSet<String>();
//    	ArrayList<String> threadMethodStmts = getThreadMethodStmts(threadFile);
//		System.out.println(" threadInvokeStmts="+threadMethodStmts);
    	ArrayList<String> threadMethodStmts = getJimpleMethodStmts(jimpleFile);
		System.out.println(" threadInvokeStmts="+threadMethodStmts);		
		HashSet<String> advDependence=new HashSet<String>();
		for (int i=0; i<6; i++)
		{
			advDependence= getThreadReadyDADependence(threadFile, "READY_DA info Value["+i+"]=Statistics for Ready dependence as calculated by edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv3","A total of ",threadMethodStmts,jimpleFile);
			allDependence.addAll(advDependence);
		}		
    	return allDependence;
    }
    public static void writeArrayToFileWithType(ArrayList<String> array1, String dest, String typeStr) {  
        FileWriter writer = null;  
        BufferedWriter bw = null;  
        String lineStr="";
        try {  
            File file = new File(dest);  
            if (!file.exists()) {  
                file.createNewFile();  
            }  
            writer = new FileWriter(dest, true); 
            bw = new BufferedWriter(writer);  
            for (int i=0; i<array1.size(); i++)
    		{
            	lineStr=array1.get(i)+" "+typeStr+"\n";
            	 bw.write(lineStr);   
    		}
            bw.close();  
            writer.close();  
   
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }  
    
    public static void writeHashSetToFileWithType(HashSet<String> hs1, String dest, String typeStr) {  
        FileWriter writer = null;  
        BufferedWriter bw = null;  
        String lineStr="";
        try {  
            File file = new File(dest);  
            if (!file.exists()) {  
                file.createNewFile();  
            }  
            writer = new FileWriter(dest, true); 
            bw = new BufferedWriter(writer);  
            for (String item:hs1)
    		{
            	lineStr=item+" "+typeStr+"\n";
            	 bw.write(lineStr);   
    		}
            bw.close();  
            writer.close();  
   
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }  
    
	public static HashSet<String> getHashFromFile(String listFile) {
		HashSet<String>  resultA = new HashSet<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
    
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = ""; 
            while ((str = br.readLine()) != null) {  
        		resultA.add(str);
            }     
            br.close();  
            reader.close();  
        } catch (Exception e) {  
            e.printStackTrace();   
                
        }  
        return resultA;
	}
	public static HashSet<String> getMethodStmtsFromFile(String listFile) {
		HashSet<String>  resultA = new HashSet<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
        String directionStr="";
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = ""; 
            while ((str = br.readLine()) != null) {  
				if (!str.contains(" <-- ") &&  !str.contains(" --> ") ) {
					continue;
				}
	    		if (str.indexOf(" <-- ")>1)
	    		{	
	    			directionStr=" <-- ";
	    		}			
	    		else
	    			directionStr=" --> ";
				String strs[]=str.split(directionStr);
				if (strs.length==0)  
				{
					resultA.add(str);
				}
				else if (strs.length==1)  
				{
					resultA.add(strs[0].trim());
				}
				else if (strs.length==2)  
				{
					resultA.add(strs[0].trim());
					resultA.add(strs[1].trim());
				}
            }     
            br.close();  
            reader.close();  
        } catch (Exception e) {  
            e.printStackTrace();   
                
        }  
        return resultA;
	}
	
	public static HashMap<Integer, HashSet<String>> getDeDtFromFile(String listFile) {
		HashSet<String>  des= new HashSet<String>(); 
		HashSet<String>  dts= new HashSet<String>(); 
        FileReader reader = null;  
        BufferedReader br = null;   
        String directionStr="";
        try {  
   
            reader = new FileReader(listFile);  
            br = new BufferedReader(reader);              
            String str = ""; 
            while ((str = br.readLine()) != null) {  
				if (!str.contains(" <-- ") &&  !str.contains(" --> ") ) {
					continue;
				}
	    		if (str.indexOf(" <-- ")>1)
	    		{	
	    			directionStr=" <-- ";
	    		}			
	    		else if (str.indexOf(" --> ")>1)
	    		{	
	    			directionStr=" --> ";
	    		}
	    		else
	    			continue;
				String strs[]=str.split(directionStr);
				if (strs.length<2)
					continue;
				System.out.println("str="+str);
				if (directionStr.equals(" --> "))
				{
					des.add(strs[0].trim());
					dts.add(strs[1].trim());
				}
				else if (directionStr.equals(" <-- "))
				{
					des.add(strs[1].trim());
					dts.add(strs[0].trim());
				}		
				System.out.println("des="+des);		
				System.out.println("dts="+dts);		
            }     
            br.close();  
            reader.close();  
        } catch (Exception e) {  
            e.printStackTrace();   
                
        }  
        HashMap<Integer, HashSet<String>> hses= new HashMap<Integer, HashSet<String>>();
        hses.put(0,des);
        hses.put(1,dts);
        return hses;
	}
}
