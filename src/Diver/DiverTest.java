package Diver;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

//import soot.*;
//import soot.jimple.*;
//import soot.util.*;
//import soot.Scene;
//import soot.jimple.spark.SparkTransformer;
//import soot.options.*;
//import soot.Pack;
//import soot.PackManager;
//import soot.PhaseOptions;
//import soot.Scene;
//import soot.SootClass;
//import soot.SootMethod;
//import soot.Transform;
//import soot.options.Options;
//import soot.*;
//import soot.tools.CFGViewer;
//import soot.util.cfgcmd.CFGOptionMatcher.CFGOption;









import dua.Forensics;
import fault.StmtMapper;
import soot.*;
import soot.util.dot.DotGraph;
import edu.ksu.cis.indus.annotations.NonNull;
import edu.ksu.cis.indus.annotations.NonNullContainer;
import edu.ksu.cis.indus.common.collections.IteratorUtils;
import edu.ksu.cis.indus.common.soot.SootPredicatesAndTransformers;
import edu.ksu.cis.indus.interfaces.IEnvironment;
import edu.ksu.cis.indus.processing.Context;
import EAS.*;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Iterator;
import java.util.Set;

import profile.InstrumManager;
import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.CFG.CFGNode;
import dua.util.Util;
import soot.jimple.*;
import EAS.EAInst;
import MciaUtil.utils;






import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.LongType;
import soot.Modifier;
import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.VoidType;
import soot.jimple.GotoStmt;
import soot.jimple.InterfaceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.NewExpr;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.util.Chain;



public class DiverTest {
	  public static Map<Stmt, Integer> sToId = null;
	  public static Stmt[] idToS = null;
	 static boolean debugOut = true;
	 public static void main(String []args) {
		System.out.println("args="+args);
//        if(args.length == 0)
//        {
//            System.out.println("Usage: java MyAnalysis class_to_analyse");
//            System.exit(0);
//        }            
//        else
//			System.out.println("[mainClass]"+args[0]);	
//        if (debugOut)
//        {	
//			System.out.println("args[0]="+args[0]);	
//			System.out.println("args[1]="+args[1]);	
//			System.out.println("args[2]="+args[2]);	
//        } 
        PackManager.v().getPack("jap").add(new Transform("jap.myTransform", new MyAnalysisdtTest()));

        //String paras[] = new String[] {"-cp",".;C:/JDK1872/jre/lib/rt.jar;C:/soot/soot-2.5.0.jar;C:/Research/duafWK1/InstrReporters/bin","-allow-phantom-refs","-pp","profile.BranchReporter"}; 
        //String paras[] = new String[] {"-cp",".;C:/JDK1872/jre/lib/rt.jar;C:/soot/soot-2.5.0.jar;C:/Soot/In","-allow-phantom-refs","-pp","C"}; 
        //String paras[] = new String[] {"-cp",".","-allow-phantom-refs","-pp","-process-dir", "C:/Soot/In"};    
        String paras[] = new String[] {"-cp",".","-allow-phantom-refs","-pp","-process-dir", "C:/Soot/In"};     
        soot.Main.main(paras);		 
    }
	 
}

class MyAnalysisdtTest extends BodyTransformer
{
	static boolean debugOut = true;
//	static HashSet<String> sources = new HashSet<>(); 
//	static HashSet<String> sinks = new HashSet<>(); 
////	static TreeSet<String> sourceMethods = new TreeSet<>(); 
////	static TreeSet<String> sinkMethods = new TreeSet<>(); 		
////	static String sourceMsg=""; 
////	static String sinkMsg="";
//	static String sourceFile=System.getProperty("user.dir") + File.separator + "source_"+System.currentTimeMillis() + ".txt";
//	static String sinkFile=System.getProperty("user.dir") + File.separator + "sink_"+System.currentTimeMillis() + ".txt";
//	static String allFile=System.getProperty("user.dir") + File.separator + "all_"+System.currentTimeMillis() + ".txt";
//	static String methodPairFile=System.getProperty("user.dir") + File.separator + "sourceSinkMethodPair_"+System.currentTimeMillis() + ".txt";
//	static String stmtPairFile=System.getProperty("user.dir") + File.separator + "sourceSinkStmtPair_"+System.currentTimeMillis() + ".txt";
//	ArrayList  sourceMethods = new ArrayList();
//	ArrayList  sinkMethods = new ArrayList();
//	ArrayList  sourceStmts = new ArrayList();
//	ArrayList  sinkStmts = new ArrayList();
    protected void internalTransform(Body myBody, String phaseName, Map options)
    {
    	String midStr1="";
		for (SootClass c:Scene.v().getApplicationClasses()) 
		//SootClass c = Scene.v().getSootClass("C");
    	//SootClass c = myBody.getMethod().getDeclaringClass();
    	//SootClass c = Scene.v().loadClassAndSupport("C");
		{   System.out.println("getApplicationClasses()="+c);
			SootMethod m1= getRunMethod(c);
			if (m1!=null)
				System.out.println(" m1="+m1);
			/*
////			if(m.isConcrete())
//			SootMethod _startMethod = c.getMethodByName("start");
//			System.out.println("_startMethod = "+_startMethod);
			for(SootMethod sMethod:c.getMethods())
			{
				if ( !sMethod.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					continue; 
				}
				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					continue;
				}
//				if (!reachableMethods.contains(sMethod)) {
//					// skip unreachable methods
//					continue;
//				}
				if ( !sMethod.hasActiveBody() ) {
					continue;
				}
				System.out.println("[sootMethod]="+sMethod+" sMethod.getName()="+sMethod.getName()+" sMethod.getSignature()="+sMethod.getSignature());
				Body b=sMethod.retrieveActiveBody();
				//System.out.println("[body]"+b);
				Iterator<Unit> stmts=b.getUnits().snapshotIterator();
				while(stmts.hasNext())
				{
					Unit u=stmts.next();
					midStr1=u.toString();
					//System.out.println("stmt="+midStr1);
				
				}
				
			}		
			*/	
		}
    }

	private SootMethod getRunMethod(final SootClass threadClass) {
		return threadClass.getMethod("run", Collections.EMPTY_LIST, VoidType.v());
	}
	
//	private Collection<Value> getRunnables(final SootClass threadClass) {
//		Collection<Value> _result = Collections.emptySet();
//		final SootMethod _threadRunMethod = getRunMethod(threadClass);
//
//		final Iterator<Unit> _units = _threadRunMethod.retrieveActiveBody().getUnits().iterator();
//		for (final Iterator<Unit> _iter = IteratorUtils.filteredIterator(_units,
//				SootPredicatesAndTransformers.INVOKING_UNIT_PREDICATE); _iter.hasNext();) {
//			final Unit _stmt = _iter.next();
//			final InvokeExpr _expr = ((InvokeStmt) _stmt).getInvokeExpr();
//			final SootMethod _invokedMethod = _expr.getMethod();
//
//			if (_expr instanceof InterfaceInvokeExpr && _invokedMethod.getName().equals("run")
//					&& _invokedMethod.getDeclaringClass().getName().equals("java.lang.Runnable")) {
//				final InterfaceInvokeExpr _iExpr = (InterfaceInvokeExpr) _expr;
//				final Context _context = new Context();
//				_context.setRootMethod(_threadRunMethod);
//				_context.setStmt((Stmt) _stmt);
//				_context.setProgramPoint(_iExpr.getBaseBox());
//				_result = analyzer.getValues(_iExpr.getBase(), _context);
//				break;
//			}
//		}
//		return _result;
//	}
//
//	/**
//	 * Retrieves the runnable methods in the given class
//	 * 
//	 * @param clazz of interest.
//	 * @return a collection of runnable methods in the given class.
//	 */
//	@NonNull @NonNullContainer Collection<SootMethod> getRunnableMethods(@NonNull final SootClass clazz) {
//		final Collection<Value> _runnables = getRunnables(clazz);
//		final IEnvironment _env = analyzer.getEnvironment();
//		final Collection<SootMethod> _result = new HashSet<SootMethod>(_runnables.size());
//		for (final Iterator<Value> _j = IteratorUtils.filteredIterator(_runnables.iterator(),
//				SootPredicatesAndTransformers.NEW_EXPR_PREDICATE); _j.hasNext();) {
//			final NewExpr _temp = (NewExpr) _j.next();
//			final SootClass _runnable = _env.getClass((_temp.getBaseType()).getClassName());
//			final SootMethod _entryPoint = getRunMethod(_runnable);
//			_result.add(_entryPoint);
//		}
//		return _result;
//	}
}