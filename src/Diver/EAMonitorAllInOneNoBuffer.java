/**
 * File: src/Diver/EAMonitorAllInOneNoBuffer.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      	Changes
 * -------------------------------------------------------------------------------------------
 * 12/31/15		hcai			created; compute all impacts during runtime, without producing traces
 * 01/03/16		hcai			added checks against deep-recursive entrance to the handlers due to recursions in the subject runtime
 * 01/05/16     hcai            added an option of computing impact set for specified queries only
*/
package Diver;

import java.io.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class EAMonitorAllInOneNoBuffer {
	protected static ImpactAllInOne icAgent = null;
	public static void setICAgent(ImpactAllInOne _agent) {icAgent = _agent;}
	
	protected static Integer preMethod = null;
	
	protected static int g_eventCnt = 0;
	
	/* a flag ensuring the initialization and termination are both executed exactly once and they are paired*/
	protected static boolean bInitialized = false;

	private static boolean active = false;
	
	private static boolean start = false;
	
	/* clean up internal data structures that should be done so for separate dumping of them, a typical such occasion is doing this per test case */
	private synchronized static void resetInternals() {
		preMethod = null;
		g_eventCnt = 0;
		start = false;
	}
	
	/* initialize the two maps and the global counter upon the program start event */		
	public synchronized static void initialize() throws Exception{
		resetInternals();
		bInitialized = true;
	}
	
	public synchronized static void enter(String methodname){
		if (active) return;
		active = true;
		try {
			System.out.println("event no. " + g_eventCnt++);
			Integer smidx = ImpactAllInOne.getMethodIdx(methodname);
			if (smidx==null || !ImpactAllInOne.isMethodInSVTG(smidx)) {
				return;
			}
			
			if (null != preMethod && preMethod == smidx) {
				return;
			}

			
			if (!start) {
				start = (ImpactAllInOne.getAllQueries()==null || ImpactAllInOne.getAllQueries().contains(smidx));
				if (!start) {
					active = false;
					return;
				}
			}
			
			// trivially each method, once executed, is treated as impacted by itself
			if (!icAgent.getAllImpactSets().containsKey(smidx)) {
				icAgent.add2ImpactSet(smidx, smidx);
			}
			
			icAgent.onMethodEntryEvent(smidx);
			
			if (null != preMethod && preMethod != smidx) {
				// close some "open" source nodes
				icAgent.closeNodes(preMethod, smidx);
			}
			
			preMethod = smidx;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			active = false;
		}
	}

	/* the callee could be either an actual method called or a trap */
	public synchronized static void returnInto(String methodname, String calleeName){
		if (active) return;
		active = true;
		try {
			System.out.println("event no. " + g_eventCnt++);
			Integer smidx = ImpactAllInOne.getMethodIdx(methodname);
			if (smidx==null || !ImpactAllInOne.isMethodInSVTG(smidx)) {
				return;
			}
			
			// trivially each method, once executed, is treated as impacted by itself
			/*
			if (!icAgent.getAllImpactSets().containsKey(smidx)) {
				icAgent.add2ImpactSet(smidx, smidx);
			}
			*/
			if (!start) {
				active = false;
				return;
			}
			
			icAgent.onMethodReturnedIntoEvent(smidx);
			
			if (null != preMethod && preMethod != smidx) {
				// close some "open" source nodes
				icAgent.closeNodes(preMethod, smidx);
			}
			
			preMethod = smidx;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			active = false;
		}
	}
	
	public synchronized static void terminate(String where) throws Exception {
		if (bInitialized) {
			bInitialized = false;
		}
		else {
			return;
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

/* vim :set ts=4 tw=4 tws=4 */

